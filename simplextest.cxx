#include <iostream>
#include <thread>
#include <list>
#include <cmath>
#include <fstream>
#include <sstream>
#include <unistd.h>

#include <noise/simplex.hxx>

std::string Usage(char* const progname);

int main(int argc, char** argv)
{
    std::string outfilename("out.ply");

    unsigned int const threadlimit = 9;

    double const threshold = 1.8;
    double const scale = 1.0;

    // X, Y, and Z respectively
    unsigned int width = 256;
    unsigned int depth = 256;
    unsigned int height = 128;

    double offset[3] = {0.0, 0.0, 0.0};

    std::list<std::thread> threads;

    unsigned int vertcount = 0;
    unsigned int printstep = 0;

    bool carveCaves = false;

    char opt;

    while ((opt = getopt(argc, argv, "d:o:s:ch")) != -1)
    {
        switch (opt)
        {
            case 'c':
            {
                carveCaves = true;
                break;
            }

            case 'd':
            {
                std::string input(optarg);

                // Take input from 0 until the first x
                std::string x(input, 0, input.find(":"));

                // Take input starting after the first x, and ending before the second x.
                std::string y(input, x.size() + 1, input.find(":", x.size() + 1) - (x.size() + 1));

                // Take input starting after the second x, and finishing at the end
                std::string z(input, x.size() + y.size() + 2, input.size() - (x.size() + y.size() + 2));

                std::stringstream(x) >> width;
                std::stringstream(y) >> depth;
                std::stringstream(z) >> height;
                break;
            }

            case 'h':
            {
                std::cout << Usage(argv[0]) << std::endl;
                return (0);

                break;
            }

            case 'o':
            {
                outfilename = std::string(optarg);
                break;
            }

            case 's':
            {
                std::string input(optarg);

                // Take input from 0 until the first x
                std::string x(input, 0, input.find(":"));

                // Take input starting after the first x, and ending before the second x.
                std::string y(input, x.size() + 1, input.find(":", x.size() + 1) - (x.size() + 1));

                // Take input starting after the second x, and finishing at the end
                std::string z(input, x.size() + y.size() + 2, input.size() - (x.size() + y.size() + 2));

                std::stringstream(x) >> offset[0];
                std::stringstream(y) >> offset[1];
                std::stringstream(z) >> offset[2];
                break;
            }

            case '?':
            {
                std::cout << Usage(argv[0]) << std::endl;
                return (1);

                break;
            }
        }
    }

    std::cout << ++printstep << ":  " << "Creating with " << width << "x" << depth << "x" << height << " grid, with an offset of ("
        << offset[0] << ", " << offset[1] << ", " << offset[2] << "), " << (carveCaves ? "": "not ") << "carving caves, saving into file "
        << outfilename << std::endl;

    std::cout << ++printstep << ":  " << "Allocating voxels" << std::endl;

    double*** grid = new double**[height];

    for (unsigned int z = 0; z < height; ++z)
    {
        grid[z] = new double*[depth];

        for (unsigned int y = 0; y < depth; ++y)
        {
            grid[z][y] = new double[width];
        }
    }

    std::cout << ++printstep << ":  " << "Calculating noise" << std::endl;

    for(unsigned int x = 0; x < width; x++)
    {
        for(unsigned int y = 0; y < depth; y++)
        {
            threads.push_back(std::thread([scale, height, width, depth, carveCaves, &offset, x, y, &grid]() -> void
            {
                for(unsigned int z = 0; z < height; z++)
                {
                    double plateauRounding;
                    // Center x and y about 0
                    double xf = (double)x / (double)width - 0.5;
                    double yf=(double)y / (double)depth - 0.5;

                    // Z remains in [0, 1)
                    double zf = (double)z / (double)height;

                    // Scale the noise, and also adjust for perspective
                    double sx = xf * scale * ((double)width / (double)height);
                    double sy = yf * scale * ((double)depth / (double)height);
                    double sz = zf * scale;

                    if(zf <= 0.9)
                    {
                        plateauRounding = 1.0;
                    } else
                    {
                        // Between 90% and 100%, linearly dissipate
                        plateauRounding = 1.0-(zf-0.9)*10.0;
                    }   

                    double centerRounding = 0.1/(
                        pow((xf)*1.5, 2) +
                        pow((yf)*1.5, 2) +
                        pow((zf - 1.0) * 0.8, 2)
                    );  


                    double density = ( 
                        Noise::Simplex::RangeOctaveNoise(5, 0.0, 1.0, sx + offset[0], sy + offset[1], sz * 0.5 + offset[2]) *
                        centerRounding *
                        plateauRounding
                    );  
                    
                    density *= pow(Noise::Simplex::RangeNoise(0.0, 1.0, sx * 3.0 + offset[0], sy * 3.0 + offset[1], sz * 3.0 + offset[2]) + 0.4, 1.8);

                    if (carveCaves)
                    {
                        double caves = pow(Noise::Simplex::RangeNoise(0.0, 1.0, sx * 3.0 + offset[0], sy * 3.0 + offset[1], sz * 5.0 + offset[2]), 3); 

                        if(caves < 0.01)
                        {
                            density = 0;
                        }
                    }

                    grid[z][y][x] = density;
                }
            }));

            if (threads.size() >= threadlimit)
            {
                for (auto& thread: threads)
                {
                    thread.join();
                }
                threads.clear();
            }
        }
    }

    for (auto& thread: threads)
    {
        thread.join();
    }
    threads.clear();

    std::cout << ++printstep << ":  " << "Counting vertices" << std::endl;

    for (unsigned int z = 0; z < height; ++z)
    {
        for (unsigned int y = 0; y < depth; ++y)
        {
            for (unsigned int x = 0; x < width; ++x)
            {
                if (grid[z][y][x] > threshold)
                {
                    if (y == 0 || grid[z][y - 1][x] <= threshold)
                    {
                        vertcount += 4;
                    }
                    
                    if (x == (width - 1) || grid[z][y][x + 1] <= threshold)
                    {
                        vertcount += 4;
                    }

                    if (y == (depth - 1) || grid[z][y + 1][x] <= threshold)
                    {
                        vertcount += 4;
                    }

                    if (x == 0 || grid[z][y][x - 1] <= threshold)
                    {
                        vertcount += 4;
                    }

                    if (z == 0 || grid[z - 1][y][x] <= threshold)
                    {
                        vertcount += 4;
                    }

                    if (z == (height - 1) || grid[z + 1][y][x] <= threshold)
                    {
                        vertcount += 4;
                    }
                }
            }
        }
    }

    std::cout << ++printstep << ":  " << "Creating ply header" << std::endl;

    std::ofstream outfile;
    outfile.open(outfilename);

    outfile << "ply\n"
              << "format ascii 1.0\n"
              << "element vertex " << vertcount << "\n"
              << "property float x\n"
              << "property float y\n"
              << "property float z\n"
              << "element face " << (vertcount / 4) << "\n"
              << "property list uchar int vertex_index" << "\n"
              << "end_header" << std::endl;

    std::cout << ++printstep << ":  " << "Printing vertices" << std::endl;
    for (unsigned int z = 0; z < height; ++z)
    {
        for (unsigned int y = 0; y < depth; ++y)
        {
            for (unsigned int x = 0; x < width; ++x)
            {
                if (grid[z][y][x] > threshold)
                {
                    double xf = (double)x - ((double)width / 2.0);
                    double yf = (double)y - ((double)depth / 2.0);
                    double zf = (double)z - ((double)height / 2.0);

                    // Looking down the positive y axis

                    if (y == 0 || grid[z][y - 1][x] <= threshold)
                    {
                        // Front face
                        outfile << xf - 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n"
                                << xf - 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n";
                    }
                    
                    if (x == (width - 1) || grid[z][y][x + 1] <= threshold)
                    {
                        // Right face
                        outfile << xf + 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n"
                                << xf + 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n";
                    }

                    if (y == (depth - 1) || grid[z][y + 1][x] <= threshold)
                    {
                        // Back face
                        outfile << xf + 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf - 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf - 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n"
                                << xf + 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n";
                    }

                    if (x == 0 || grid[z][y][x - 1] <= threshold)
                    {
                        // Left face
                        outfile << xf - 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf - 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n"
                                << xf - 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n"
                                << xf - 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n";
                    }

                    if (z == 0 || grid[z - 1][y][x] <= threshold)
                    {
                        // Bottom face
                        outfile << xf - 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n"
                                << xf - 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf + 0.5 << " " << zf - 0.5 << "\n"
                                << xf + 0.5 << " " << yf - 0.5 << " " << zf - 0.5 << "\n";
                    }

                    if (z == (height - 1) || grid[z + 1][y][x] <= threshold)
                    {
                        // Top face
                        outfile << xf - 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n"
                                << xf - 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n"
                                << xf + 0.5 << " " << yf - 0.5 << " " << zf + 0.5 << "\n"
                                << xf + 0.5 << " " << yf + 0.5 << " " << zf + 0.5 << "\n";
                    }
                }
            }
        }
    }

    std::cout << ++printstep << ":  " << "Printing faces" << std::endl;

    for (unsigned int currentvert = 0; currentvert < vertcount;)
    {

        outfile << "4 " << currentvert << " " << (currentvert + 1) << " " << (currentvert + 2) << " " << (currentvert + 3) << "\n";

        currentvert += 4;
    }

    outfile << std::flush;
    outfile.close();

    std::cout << ++printstep << ":  " << "Deleting voxels" << std::endl;

    for (unsigned int z = 0; z < height; ++z)
    {
        for (unsigned int y = 0; y < depth; ++y)
        {
            delete[] (grid[z][y]);
        }

        delete[] (grid[z]);
    }

    delete[] (grid);

    std::cout << ++printstep << ":  " << "Finished" << std::endl;
    return (0);
}

std::string Usage(char* const progname)
{
    std::string help = "Usage:\n\t";
    help.append(progname);
    help.append(" {-d dimensions} {-o outfilepath} {-s shift}\n"
            "\t\t-d\tDimensions of the grid that holds the noise values.  Effects the size of the island\n"
              "\t\t\tand the complexity of the output.  Integer only.  Default is 256x256x128.\n"
            "\t\t-o\tThe path to the output file\n"
            "\t\t-s\tShift value.  Does not change offset position of island, but the shift of the noise\n"
              "\t\t\tvalues.  Randomizing this well (within a low range) will produce a good unique island.\n"
              "\t\t\tTakes floating point input.\n"
            "\t\t-c\tCarve caves.\n\n"
            "\t\t-h\tDisplay this menu and quit.\n\n"
              "\t\t\tOptions expecting coordinate triplets are specified in a colon-delemited list (eg. 54:-12.345:24.36)");
    return (std::move(help));
}
