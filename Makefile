BUILD = Release
objects = simplex.o noisetest.o
testfiles = simplextest
libraries = libnoise.so
CXX = g++

#CommonOpts = -Wall -Wextra -std=c++11 
CommonOptsAll = -Wall -std=c++11 -pthread -fPIC
CommonDebugOpts = -ggdb -O0
CommonReleaseOpts = -s -O3 -march=native
CommonOpts = $(CommonOptsAll) $(Common$(BUILD)Opts)

CompileOptsAll = -c $(CommonOpts) -I.
CompileOptsRelease =  
CompileOptsDebug = 
CompileOpts = $(CompileOptsAll) $(CompileOpts$(BUILD))

LinkerOptsAll = $(CommonOpts)
LinkerOptsRelease = 
LinkerOptsDebug = 
LinkerOpts = $(LinkerOptsAll) $(LinkerOpts$(BUILD))

.PHONY : all clean

all : $(libraries)

simplextest : simplextest.o simplex.o
	$(CXX) $(LinkerOpts) -o ./simplextest simplextest.o simplex.o
clean :
	-rm $(objects) $(testfiles) $(libraries) 
test : flaot
	./flaot

libnoise.so : simplex.o
	$(CXX) -shared $(LinkerOpts) -o ./libnoise.so simplex.o

simplextest.o : simplextest.cxx simplex.o
	$(CXX) $(CompileOpts) -o ./simplextest.o ./simplextest.cxx

simplex.o : noise/simplex.cxx noise/simplex.hxx
	$(CXX) $(CompileOpts) -o ./simplex.o ./noise/simplex.cxx
