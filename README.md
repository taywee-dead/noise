Noise
=====

Liberally licensed noise generation library.  Currently only supports Perlin's Simplex Noise, with some permutations.
