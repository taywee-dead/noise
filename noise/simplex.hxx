/*
 * This is the Stefan Gustavson algorithm ported directly to C++.
 * Convenience functions like Octave noise are also added.
*
 * Original Header:
 *
 * A speed-improved simplex noise algorithm for 2D, 3D and 4D in Java.
 *
 * Based on example code by Stefan Gustavson (stegu@itn.liu.se).
 * Optimisations by Peter Eastman (peastman@drizzle.stanford.edu).
 * Better rank ordering method by Stefan Gustavson in 2012.
 *
 * This could be speeded up even further, but it's useful as it is.
 *
 * Version 2012-03-09
 *
 * This code was placed in the public domain by its original author,
 * Stefan Gustavson. You may use it as you see fit, but
 * attribution is appreciated.
 *
 */
#ifndef NOISE_SIMPLEX_HXX
#define NOISE_SIMPLEX_HXX

#include <cmath>

namespace Noise
{
    class Simplex
    {
    private:
        // Inner class to speed up gradient computations
        // (array access is a lot slower than member access)
        struct Grad
        {
            Grad(double x, double y, double z);

            Grad(double x, double y, double z, double w);

            double x, y, z, w;
        };

        const static Grad grad3[12];
        const static Grad grad4[32];
        const static unsigned char perm[512];

        static unsigned char permMod12[512];
        static bool permMod12Computed;

        static void ComputePermMod12();

        // Skewing and unskewing factors for 2, 3, and 4 dimensions
        static double const F2;
        static double const G2;
        static double const F3;
        static double const G3;
        static double const F4;
        static double const G4;

    public:

        // This method is a *lot* faster than using (int)Math.floor(x)
        inline static int FastFloor(double x);

        static double Dot(Grad const & g, double x, double y);

        static double Dot(Grad const & g, double x, double y, double z);

        static double Dot(Grad const & g, double x, double y, double z, double w);


        /** 2D simplex Noise
         *  
         *  Ranged from -1.0 to 1.0
         */
        static double Noise(double xin, double yin);

        /** 3D simplex Noise
         *  
         *  Ranged from -1.0 to 1.0
         */
        static double Noise(double xin, double yin, double zin);

        /** 4D simplex Noise
         *  
         *  Ranged from -1.0 to 1.0
         */
        static double Noise(double x, double y, double z, double w);

        /** 2D simplex Noise
         *  
         *  Ranged from min to max
         */
        static double RangeNoise(double min, double max, double xin, double yin);

        /** 3D simplex Noise
         *  
         *  Ranged from min to max
         */
        static double RangeNoise(double min, double max, double xin, double yin, double zin);

        /** 4D simplex Noise
         *  
         *  Ranged from min to max
         */
        static double RangeNoise(double min, double max, double x, double y, double z, double w);

        /** 2D simplex Octave noise
         *  
         *  Ranged from -octaves to octaves
         */
        static double OctaveNoise(unsigned int octaves, double xin, double yin);

        /** 3D simplex Octave noise
         *  
         *  Ranged from -octaves to octaves
         */
        static double OctaveNoise(unsigned int octaves, double xin, double yin, double zin);

        /** 4D simplex Octave noise
         *  
         *  Ranged from -octaves to octaves
         */
        static double OctaveNoise(unsigned int octaves, double x, double y, double z, double w);

        /** 2D simplex Octave noise
         *  
         *  Ranged from (octaves * min) to (octaves * max)
         */
        static double RangeOctaveNoise(unsigned int octaves, double min, double max, double xin, double yin);

        /** 3D simplex Octave noise
         *  
         *  Ranged from (octaves * min) to (octaves * max)
         */
        static double RangeOctaveNoise(unsigned int octaves, double min, double max, double xin, double yin, double zin);

        /** 4D simplex Octave noise
         *  
         *  Ranged from (octaves * min) to (octaves * max)
         */
        static double RangeOctaveNoise(unsigned int octaves, double min, double max, double x, double y, double z, double w);
    };
}

#endif
